package com.fauzi.smsretrieverskeleteton.smsretrieverhelper

interface OTPReceiveInterface {
        fun onOtpReceived(otp : String)
        fun onOtpTimeout()
}
