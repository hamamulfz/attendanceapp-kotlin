package com.fauzi.smsretrieverskeleteton.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.fauzi.smsretrieverskeleteton.R
import com.fauzi.smsretrieverskeleteton.verification.VerificationActivity
import com.fauzi.smsretrieverskeleteton.smsretrieverhelper.AppSignatureHelper
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        var appSignature = AppSignatureHelper(this)
        appSignature.getAppSignatures()

        login_button.setOnClickListener {
            startSMSListener()
            val intent = Intent(this, VerificationActivity::class.java)
            startActivity(intent)
        }
    }

    private fun startSMSListener() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {
            Toast.makeText(this, "SMS Retriever starts", Toast.LENGTH_SHORT).show()
        }

        task.addOnFailureListener {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }
}
