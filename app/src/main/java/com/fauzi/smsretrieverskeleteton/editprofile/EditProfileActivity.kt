package com.fauzi.smsretrieverskeleteton.editprofile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.fauzi.smsretrieverskeleteton.R

class EditProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
    }
}
