package com.fauzi.smsretrieverskeleteton.verification

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.fauzi.smsretrieverskeleteton.location.LocationActivity
import com.fauzi.smsretrieverskeleteton.R
import com.fauzi.smsretrieverskeleteton.smsretrieverhelper.MySMSBroadcastReceiver
import com.fauzi.smsretrieverskeleteton.smsretrieverhelper.OTPReceiveInterface
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import kotlinx.android.synthetic.main.activity_verification.*
import kotlin.random.Random

class VerificationActivity : AppCompatActivity(),
    OTPReceiveInterface {


    private val mySMSBroadcastReceiver = MySMSBroadcastReceiver()
    private var randomNumber : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        randomNumber = Random.nextInt(1000, 9999)
        generated_code.text = randomNumber.toString()

        //set otp callback from broadcast receiver
        mySMSBroadcastReceiver.setOnOtpListeners(this)

        //Registering Receiver
        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        applicationContext.registerReceiver(mySMSBroadcastReceiver, intentFilter)

        resend_otp.setOnClickListener {
            randomNumber = Random.nextInt(1000, 9999)
            generated_code.text = randomNumber.toString()
            startSMSListener()
        }

        submit_verification_code.setOnClickListener {
            verifyOtp(otp_field.text.trim().toString())
        }

        otp_field.addTextChangedListener(object : TextWatcher{
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (otp_field.text.trim().isNotEmpty()){
                submit_verification_code.isEnabled = true
            } else {
                submit_verification_code.isEnabled = false
            }
        }
    })
}

    //check sms retrieval client
    private fun startSMSListener() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {
            Toast.makeText(this, "SMS Retriever starts", Toast.LENGTH_LONG).show()
        }

        task.addOnFailureListener {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }
    }

    override fun onOtpReceived(otp: String) {
//        tvOtp.text = "OTP is : $otp"
        otp_field.setText(otp)
        verifyOtp(otp)
    }

    fun verifyOtp(otp: String){
        if (otp.toInt() == randomNumber){
            startActivity(Intent(this, LocationActivity::class.java))
        }
    }

    override fun onOtpTimeout() {
        otp_field.setText("Time out, please resend")
        submit_verification_code.isEnabled = false
    }

}
